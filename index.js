const express = require('express')
var session = require('express-session')
var exphbs = require('express-handlebars');
const SECRET = "1332$###SECRET###222"

const app = express()
const db = require('./api/configs/sequelize')

app.engine('handlebars', exphbs({
    defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

//Criação da seção
app.use(session({
    secret: SECRET,
    name: 'meutesteaula',
    cookie: { maxAge: 60000 }
})
);

app.get('/', (req, res) => {
    res.render('login')
})

require('./api/user/routes')(app)

db.sync();

var server = app.listen(3000, () => {
    console.log("Servidor rodando na porta 3000 no host " + server.address.address)
})