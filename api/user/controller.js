const httpStatus = require('http-status')
const User = require("./model")
const bcrypt = require('bcrypt')

exports.create = async (req, res) => {

    let pass = await bcrypt.hash(req.body.password, 10);

    try {
        let user = await User.create({
            name: req.body.name,
            email: req.body.email,
            login: req.body.login,
            password: pass
        });
        res.send(user);
    } catch (err) {
        console.log(err);
    }
}


exports.findAll = async (req, res) => {
    try {
        let users = await User.findAll();
        res.send(users);
    } catch (err) {
        res.status(err.status).end(err.message);
    }
}

exports.login = async (req, res) => {

    try {

        let user = await User.findOne({
            where: { login: req.body.login }
        });

        if (user) {
            bcrypt.compare(req.body.password, user.password, function (err, result) {
                if (result) {

                    req.session.user = user.id //insere o id do usuário na sessão
                    res.json(user);

                } else {
                    res.status(httpStatus.UNAUTHORIZED);
                    res.send("Usuário e/ou senha inválidos");
                }
            });
        } else {
            res.status(httpStatus.UNAUTHORIZED);
            res.send("Usuário e/ou senha inválidos");
        }

    } catch (err) {

    }

}

exports.logout = async (req, res) => {

    req.session.destroy(function (err) {
        return res.redirect('/');
    })
}
