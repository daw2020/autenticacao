const session = require('../auth/session')

module.exports = (app) => {

    const controller = require('./controller')

    //Criar um novo perfil de usuário
    app.post('/user', controller.create)

    //Busca todos os perfis de usuário
    app.get('/user', session, controller.findAll)

    //Login
    app.post('/login', controller.login)

    //Logout
    app.get('/logout', controller.logout)

}