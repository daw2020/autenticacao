module.exports = function (req, res, next) {
    if (!req.session.user) {
        console.log("Sessão inexistente ou finalizada.")
        return res.redirect('/');
    }
    return next();
};